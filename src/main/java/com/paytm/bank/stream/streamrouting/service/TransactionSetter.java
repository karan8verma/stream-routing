package com.paytm.bank.stream.streamrouting.service;

import com.paytm.bank.stream.streamrouting.model.requests.Transaction;
import com.paytm.bank.stream.streamrouting.model.responses.TransactionResponse;

@FunctionalInterface
public interface TransactionSetter {
  TransactionResponse getResponse(Transaction history);
}
