package com.paytm.bank.stream.streamrouting.model.responses;

import com.paytm.bank.stream.streamrouting.model.requests.Transaction;
import lombok.Data;

@Data
public class TransactionResponse {

  String  type;
  Details details;
  String  txntext;


  public void setTransactionDetails(Transaction transactionDetails) {
    this.details = new Details(transactionDetails);
  }
}
