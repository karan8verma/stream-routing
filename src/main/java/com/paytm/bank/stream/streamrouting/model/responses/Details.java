package com.paytm.bank.stream.streamrouting.model.responses;

import com.paytm.bank.stream.streamrouting.model.requests.Transaction;
import lombok.Data;

@Data
public class Details {

  private String nbOld ;
  private String amount;
  private String merchantName;
  private String cbsTxnId;
  private String rrn;
  private String timestamp;
  private String customerCustId;
  private String merchantId;
  private String merchantMCC;


  public Details(Transaction transactionDetails) {

    //set Details;



  }
}
