package com.paytm.bank.stream.streamrouting.service;

import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

@Service
public interface TransformerService {

  Message<?> transform(Object message);

}
