package com.paytm.bank.stream.streamrouting.config;


import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.common.LiteralExpression;
import org.springframework.integration.kafka.outbound.KafkaProducerMessageHandler;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

@Configuration
public class KafkaProducerConfig {


  @Value("${spring.kafka.bootstrap-servers}")
  private String bootStrapServer;


  @Bean
  public KafkaProducerMessageHandler kafkaMessageHandler() {
    KafkaProducerMessageHandler kafkaProducerMessageHandler = new KafkaProducerMessageHandler(
        kafkaTemplate());
    kafkaProducerMessageHandler.setTopicExpression(new LiteralExpression("test1"));
    kafkaProducerMessageHandler.setMessageKeyExpression(new LiteralExpression("kafka-integration"));
    return kafkaProducerMessageHandler;
  }


  @Bean
  public KafkaTemplate kafkaTemplate() {

    KafkaTemplate kafkaTemplate = new KafkaTemplate(producerFactory());
    return kafkaTemplate;
  }

  @Bean
  public ProducerFactory producerFactory() {
    return new DefaultKafkaProducerFactory(producerConfigs());
  }

  @Bean
  public Map producerConfigs() {
    Map properties = new HashMap();
    properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServer);
    properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
    properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
    // introduce a delay on the send to allow more messages to accumulate
    properties.put(ProducerConfig.LINGER_MS_CONFIG, 1);

    return properties;
  }


}
