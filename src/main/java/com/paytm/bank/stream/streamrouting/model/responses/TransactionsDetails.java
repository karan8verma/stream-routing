package com.paytm.bank.stream.streamrouting.model.responses;

import com.paytm.bank.stream.streamrouting.model.requests.Transaction;
import lombok.Data;

@Data
public class TransactionsDetails {
  private String amount;
  private String merchantName;
  private String cbsTxnId;
  private String rrn;
  private String timestamp;
  private String customerCustId;
  private String merchantID ;
  private String merchantMCC;

  public TransactionsDetails getDetails(Transaction transactionHistory){
    TransactionsDetails transactionsDetails =  new TransactionsDetails();
    //TODO set transaction  detail fields

    return  transactionsDetails;
  }
}
