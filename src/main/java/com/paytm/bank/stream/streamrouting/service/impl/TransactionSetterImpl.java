package com.paytm.bank.stream.streamrouting.service.impl;

import com.paytm.bank.stream.streamrouting.constants.TransactionType;
import com.paytm.bank.stream.streamrouting.model.requests.Transaction;
import com.paytm.bank.stream.streamrouting.model.responses.TransactionResponse;
import com.paytm.bank.stream.streamrouting.service.TransactionSetter;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Service;

@Service
public class TransactionSetterImpl {

  Map<String, TransactionSetter> setterMap = new HashMap<>();

  @PostConstruct
  private void methodSetter() {
    setterMap.put("60202", transaction -> {
      TransactionResponse response = new TransactionResponse();
      response.setType(TransactionType.NETBANKING_PG_OLD.getType());
      response.setTransactionDetails(transaction);
      response.setTxntext(setTxnText(transaction);
      return response;
    });

    setterMap.put("80203", transaction -> {
      TransactionResponse response = new TransactionResponse();
      response.setType(TransactionType.NETBANKING_MOTO_SUB.getType());
      response.setTransactionDetails(transaction);
      return response;
    });

    setterMap.put("60301", transaction -> {
      TransactionResponse response = new TransactionResponse();
      response.setType(TransactionType.NETBANKING_REFUND_PG_OLD.getType());
      response.setTransactionDetails(transaction);
      return response;
    });

    setterMap.put("60225", transaction -> {
      TransactionResponse response = new TransactionResponse();
      response.setType(TransactionType.NETBANKING_NEW.getType());
      response.setTransactionDetails(transaction);
      return response;
    });

    setterMap.put("60226", transaction -> {
      TransactionResponse response = new TransactionResponse();
      response.setType(TransactionType.NETBANKING_REFUND_NEW.getType());
      response.setTransactionDetails(transaction);
      return response;
    });

    setterMap.put("60227", transaction -> {
      TransactionResponse response = new TransactionResponse();
      response.setType(TransactionType.NETBANKING_DIRECT_MERCHANT.getType());
      response.setTransactionDetails(transaction);
      return response;
    });

    setterMap.put("60228", transaction -> {
      TransactionResponse response = new TransactionResponse();
      response.setType(TransactionType.NETBANKING_DIRECT_MERCHANT_REFUND.getType());
      response.setTransactionDetails(transaction);
      return response;
    });
  }

  private String setTxnText(
      Transaction transaction) {
    return null;
  }

}
