package com.paytm.bank.stream.streamrouting.constants;

import lombok.Getter;

@Getter
public enum TransactionType {

  NETBANKING_PG_OLD("NB Old"),
  NETBANKING_MOTO_SUB("NB Moto Subscription"),
  NETBANKING_REFUND_PG_OLD("NB Negative Old"),
  NETBANKING_NEW("NB New"),
  NETBANKING_REFUND_NEW("NB Negative New"),
  NETBANKING_DIRECT_MERCHANT("NB Merchant New"),
  NETBANKING_DIRECT_MERCHANT_REFUND("NB Merchant Negative New");


  String type;

  TransactionType(String type) {
    this.type=type;
  }
}
