package com.paytm.bank.stream.streamrouting;

import com.paytm.bank.stream.streamrouting.service.TransformerService;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.context.IntegrationFlowContext;
import org.springframework.integration.kafka.inbound.KafkaMessageDrivenChannelAdapter;
import org.springframework.integration.kafka.outbound.KafkaProducerMessageHandler;

@SpringBootApplication
@EnableAutoConfiguration
@SpringBootConfiguration
public class StreamRoutingApplication {

  @Autowired
  IntegrationFlowContext integrationFlowContext;

  @Autowired
  ApplicationContext context;

  @Autowired
  KafkaProducerMessageHandler kafkaMessageHandler;

  @Autowired
  @Qualifier("promoTransformer")
  TransformerService promoTransformer;


  @Autowired
  KafkaMessageDrivenChannelAdapter kafkaMessageDrivenChannelAdapter;


  public static void main(String[] args) {
    SpringApplication.run(StreamRoutingApplication.class);
  }


  @PostConstruct
  private void setup() {

    System.out.println("Inside ConsumerApplication run method...");
    IntegrationFlow flow = IntegrationFlows.from(kafkaMessageDrivenChannelAdapter)
        .publishSubscribeChannel(publishSubscribeSpec ->
                publishSubscribeSpec.
                    subscribe(sub -> sub
                        .transform(m-> promoTransformer.transform(m))
                        .handle( kafkaMessageHandler))
                    .subscribe(
                        sub -> sub.handle(System.out::println)
                    )
        )
        .get();

    integrationFlowContext.registration(flow).register();
  }


}




