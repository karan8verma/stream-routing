package com.paytm.bank.stream.streamrouting.model.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Transaction extends BaseRequest {

  @JsonProperty( "tran_date")
  private String tranDate;

  @JsonProperty( "value_date")
  private String valueDate;

  @JsonProperty( "pstd_date")
  private String pstdDate;

  @JsonProperty( "tran_id")
  private String tranId;

  @JsonProperty( "tran_amt")
  private String tranAmt;

  @JsonProperty( "part_tran_type")
  private String partTranType;

  @JsonProperty( "part_tran_srl_num")
  private String partTranSrlNum;

  @JsonProperty( "rpt_code")
  private String rptCode;

  @JsonProperty( "tran_particular")
  private String tranParticular;

  @JsonProperty( "sol_id")
  private String solId;

  @JsonProperty( "acid")
  private String acid;

  @JsonProperty( "dcc_type")
  private String dccType;

  @JsonProperty( "dcc_id")
  private String dccId;

  @JsonProperty( "dcc_src_name")
  private String dccSrcName;

  @JsonProperty( "dcc_tran_id")
  private String dccTranId;

  @JsonProperty( "longitude")
  private String longitude;

  @JsonProperty( "latitude")
  private String latitude;

  @JsonProperty( "merchant_id")
  private String merchantId;

  @JsonProperty( "merchant_name")
  private String merchantName;

  @JsonProperty( "address_1")
  private String address1;

  @JsonProperty( "address_2")
  private String address2;

  @JsonProperty( "address_3")
  private String address3;

  @JsonProperty( "pin_code")
  private String pinCode;

  @JsonProperty( "free_field1")
  private String freeField1;

  @JsonProperty( "benef_name")
  private String benefName;

  @JsonProperty( "benef_acct_num")
  private String benefAcctNum;

  @JsonProperty( "benef_ifsc")
  private String benefIfsc;

  @JsonProperty( "rem_name")
  private String remName;

  @JsonProperty( "rem_acct_num")
  private String remAcctNum;

  @JsonProperty( "rem_ifsc")
  private String remIfsc;

  @JsonProperty( "rrn_num")
  private String rrnNum;

  @JsonProperty( "dc_mrchnt_id")
  private String dcMrchntId;

  @JsonProperty( "dc_mrchnt_term_id")
  private String dcMrchntTermId;

  @JsonProperty( "dc_mrchnt_nm_loc")
  private String dcMrchntNmLoc;

  @JsonProperty( "imps_msg_id")
  private String impsMsgId;

  @JsonProperty( "imps_tran_rmrks")
  private String impsTranRmrks;

  @JsonProperty( "benef_mmid")
  private String benefMmid;

  @JsonProperty( "benef_mobile")
  private String benefMobile;

  @JsonProperty( "rem_mmid")
  private String remMmid;

  @JsonProperty( "rem_mobile")
  private String remMobile;

  @JsonProperty( "mwtranid")
  private String mwTranid;
  
  @JsonProperty( "fetchtime_limit")
  private String fetchTimeLimit;
  
  @JsonProperty( "fetchtime")
  private String fetchTime;


}
