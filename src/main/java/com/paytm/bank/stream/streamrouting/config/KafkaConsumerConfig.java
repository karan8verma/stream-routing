package com.paytm.bank.stream.streamrouting.config;

import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.kafka.inbound.KafkaMessageDrivenChannelAdapter;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.ContainerProperties;

@Configuration
public class KafkaConsumerConfig {


  @Value("${spring.kafka.bootstrap-servers}")
  private String bootStrapServer;

  @Bean
  public KafkaMessageDrivenChannelAdapter kafkaMessageDrivenChannelAdapter() {

    KafkaMessageDrivenChannelAdapter kafkaMessageDrivenChannelAdapter
        = new KafkaMessageDrivenChannelAdapter(kafkaListenerContainer());
    return kafkaMessageDrivenChannelAdapter;
  }


  @SuppressWarnings("unchecked")
  @Bean
  public ConcurrentMessageListenerContainer kafkaListenerContainer() {
    ContainerProperties properties = new ContainerProperties("test");
    return new ConcurrentMessageListenerContainer(consumerFactory(), properties);
  }

  @Bean
  public ConsumerFactory consumerFactory() {
    return new DefaultKafkaConsumerFactory(consumerConfigs());
  }


  @Bean
  public Map consumerConfigs() {
    Map properties = new HashMap();
    properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServer);
    properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    properties.put(ConsumerConfig.GROUP_ID_CONFIG, "test");
    return properties;
  }

}
