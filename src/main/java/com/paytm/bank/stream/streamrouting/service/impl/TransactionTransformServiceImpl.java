package com.paytm.bank.stream.streamrouting.service.impl;

import com.google.gson.Gson;
import com.paytm.bank.stream.streamrouting.model.requests.Transaction;
import com.paytm.bank.stream.streamrouting.model.responses.TransactionResponse;
import com.paytm.bank.stream.streamrouting.service.TransformerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Service;

@Service
@Qualifier("promoTransformer")
public class TransactionTransformServiceImpl implements TransformerService {

  Logger LOGGER = LoggerFactory.getLogger(TransactionTransformServiceImpl.class);

  @Autowired
  TransactionSetterImpl transactionSetter;

  @Override
  public Message<?> transform(Object message) {
    Transaction history = getTransaction(message);
    TransactionResponse response = transactionSetter.setterMap.get(history.getRptCode()).getResponse(history);
    return new GenericMessage(response);
  }

  private Transaction getTransaction(Object message) {
    try {
      Gson gson = new Gson();
      return gson.fromJson(message.toString(), Transaction.class);
    } catch (Exception e) {
      LOGGER.error("JSON Parsing Failed with Exception {}", e);
    }
    return null;
  }
}
