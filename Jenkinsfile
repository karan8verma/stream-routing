#!groovy
def jobName = 'middleware-streamrouting'
def dockerImage = 'none'
node {

    deleteDir()

    stage('Pre Build') {
        try {
            checkout scm
            echo "Cloning code from git branch ${env.BRANCH_NAME}"
        }
        catch (exc) {
            echo "unable to pull code from branch ${env.BRANCH_NAME}"
            throw exc
        }
        try {
            echo "running prebuild.sh"
            sh 'docker/prebuild.sh'
        }
        catch (exc) {
            u echo "prebuild.sh failed to run"
            throw exc
        }
    }
    stage('Build') {
        try {

            withMaven(maven: 'Maven', jdk: 'java8') {
                echo "Running Build Environment"
                sh 'docker/build.sh'
            }
        }
        catch (exc) {
            echo "Build failed"
            throw exc
        }
    }
    stage('Package') {
        try {
            echo 'creating docker image'
            gitCommit = gitCommitId()
            Newrelic()
            sh "docker/package.sh ${gitCommit}"
            echo 'Docker Image created successfully'
        }
        catch (exc) {
            echo "Docker Image creation failed"
            throw exc
        }
    }

    stage('Push') {
        try {
            echo "Pushing the image to repo"
            gitCommit = gitCommitId()
            sh "docker/push.sh ${gitCommit}"
            echo "Docker image pushed successfully in the repo"
        }
        catch (exc) {
            echo "Unable to push image to repo"
            throw exc
        }
    }

    stage("checkout infrastructure-as-code for deployment ") {

        def env = ['ite', 'ite2', 'pt']
        def replica = ['1', '1', '4']

        gitCommit = gitCommitId()
        getVersion = getVersion()

        ws("/var/lib/jenkins/workspace/DeploymentPipeline${jobName}") {
            //Checkout function to checkout infrastructure as code for deployment
            checkout([$class: 'GitSCM', branches: [[name: '*/master']], userRemoteConfigs: [[url: 'git@bitbucket.org:paytmteam/infrastructure-as-code.git', credentialsId: 'bf57f39b-cfe0-4ac4-85e1-d8fff5d71a0b']]])

            for (i = 0; i < env.size(); i++) {
                def userInput = input(
                        id: 'userInputForDeployment', message: "Build is successful. Let\'s promote to ${env[i]}?", parameters: [
                        [$class: 'BooleanParameterDefinition', defaultValue: false, description: "Deploy to ${env[i]}", name: env[i]],
                ])

                if (userInput) {

                    stage("Deploy To ${env[i]}") {
                        try {
                            echo "Running deployment on ${env[i]}. Using docker image ${getVersion}_${gitCommit}"

                            if (searchYaml(jobName)) {

                                def file = readYaml file: "./kubernetes/${jobName}.yaml"
                                for (j = 0; j < file.size(); j++) {

                                    if (file[j].get('kind') == 'Deployment') {
                                        dockerImage = file[j].spec.template.spec.containers[0].image
                                    }
                                }
                                dockerImage = dockerImage.split(':')

                                //shell scripts used for deployment
                                sh "mkdir -p ./kubernetes/deploy${env[i]}"
                                sh "touch ./kubernetes/deploy${env[i]}/${jobName}.yaml"
                                sh "cp ./kubernetes/${jobName}.yaml ./kubernetes/deploy${env[i]}/"
                                sh "sed -i 's/iteenv/${env[i]}/g' ./kubernetes/deploy${env[i]}/${jobName}.yaml"
                                sh "sed -i 's/${dockerImage[1]}/${getVersion}_${gitCommit}/g' ./kubernetes/deploy${env[i]}/${jobName}.yaml"
                                sh "sed -i 's/replicas: 1/replicas: ${replica[i]}/g' ./kubernetes/deploy${env[i]}/${jobName}.yaml"
                                sh "sudo kubectl apply -f ./kubernetes/deploy${env[i]}/${jobName}.yaml --kubeconfig=/root/kubeconfig/kubeconfig-${env[i]}"
                            }
                        }
                        catch (exc) {
                            echo "Unable to deploy on ${env[i]}"
                            throw exc
                        }
                    }
                }
            }
        }
    }

}

def gitCommitId() {
    gitCommit = sh(returnStdout: true, script: 'git rev-parse HEAD').trim()
    shortCommit = gitCommit.take(6)
    return shortCommit
}

def getVersion() {
    version = sh(returnStdout: true, script: 'cat ./target/classes/META-INF/build-info.properties | grep build.version | cut -d\'=\' -f2').trim()
    return version
}

def searchYaml(jobName) {
    stdout = sh(script: 'ls ./kubernetes', returnStdout: true)
    jobNameSearch = jobName + '.yaml'
    JobNameExist = checkInList(stdout, jobNameSearch)
    if (JobNameExist) {
        echo "using ${jobName}.yaml"
        return true
    } else {
        echo "${jobName}.yaml not found, Deployment failed"
        return false
    }
}

def checkInList(stdout, check) {
    def b = stdout.split()
    def value = b.contains(check)
    return value
}

def changeNewrelic(key) {
    sh "mkdir -p ./newrelic/"
    sh "cp ./docker/newrelic.yml ./newrelic/newrelic.yml"
    sh "sed -i 's/ChangeKEY/${key}/g' ./newrelic/newrelic.yml"
}

def Newrelic() {
    sh "mkdir -p ./newrelic/"
    sh "cp ./docker/newrelic.yml ./newrelic/newrelic.yml"
}
