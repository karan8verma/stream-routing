#!/usr/bin/env bash
. /apps/$PROJECTNAME/bin/jvm.sh
java -Dspring.profiles.active=${BANK_STREAMROUTING_SERVICE_ENV} $JAVA_OPTS -jar /apps/$PROJECTNAME/lib/${PROJECTNAME}.jar
