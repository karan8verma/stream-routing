#!/usr/bin/env bash
. docker/utils.sh
if commandExecutor  ". docker/getversion.sh";then
  commandExecutor "sudo docker build -f docker/Dockerfile -t dockerregistry.paytmpb.io/paytmbank/middleware-streamrouting:${version}_${1} ."
else
  ret 1
fi
