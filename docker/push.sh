#!/usr/bin/env bash
. docker/utils.sh
if commandExecutor  ". docker/getversion.sh";then
  commandExecutor "sudo docker images dockerregistry.paytmpb.io/paytmbank/middleware-streamrouting:${version}_${1}" "sudo docker push dockerregistry.paytmpb.io/paytmbank/middleware-streamrouting:${version}_${1}"
else
  ret 1
fi
