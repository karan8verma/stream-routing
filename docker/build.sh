#!/usr/bin/env bash
. docker/utils.sh
mvn clean install -DskipTests
mvn deploy -DskipTests

if commandExecutor  ". docker/getversion.sh";then
  search './target/' "middleware-stream-routing.jar"
else
  ret 1
fi
