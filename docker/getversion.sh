#!/usr/bin/env bash
# meant to be included by another script
# queries version and puts in variable
. docker/utils.sh
PROJECT_DIR="$( pwd )"

if search $PROJECT_DIR/target/classes/META-INF/ build-info.properties;then
  version=$(cat $PROJECT_DIR/target/classes/META-INF/build-info.properties | grep build.version | cut -d'=' -f2)
else
  return 1
fi
